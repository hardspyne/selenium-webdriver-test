package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import java.util.List;
import java.util.Optional;

public abstract class BasePage {

    private By topMenuElements = By.xpath("//ul[@class=\"navigation-menu-items initialized\"]/li/a");
    private By logo = By.xpath("//img[@title=\"Wiley\"]");
    private By header = By.xpath("//div[contains(@class,\"hero-banner\")]");
    private By searchButton = By.xpath("//form[@name=\"search_form_SearchBox\"]//button");
    private By searchInput = By.xpath("//form[@name=\"search_form_SearchBox\"]//input");
    private By searchResultArea = By.xpath("//aside[@id=\"ui-id-2\"]");
    private By searchResultSuggestions = By.xpath("//div[@class=\"search-list\"]//a");
    private By searchResultProducts = By.xpath("//*[contains(@class,\"products-section\")]//div//span");

    WebDriver driver;

    BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public abstract String getHeaderName();

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    public WebElement getHeader() {
        return driver.findElement(header);
    }

    public List<WebElement> getTopMenuElements() {
        return driver.findElements(topMenuElements);
    }

    public void clickOnLogo() {
        driver.findElement(logo).click();
    }

    public void clickOnSearchButton() {
        driver.findElement(searchButton).submit();
    }

    public void addTextToSearchInput(String text) {
        driver.findElement(searchInput).sendKeys(text);
    }

    public WebElement getSearchResultArea() {
        return driver.findElement(searchResultArea);
    }

    public List<WebElement> getSearchSuggestions() {
        return driver.findElements(searchResultSuggestions);
    }

    public List<WebElement> getProductsSectionItems() {
        return driver.findElements(searchResultProducts);
    }

    public List<WebElement> getSubHeaderElements(String topMenuName) {
        By topMenuElement = By.xpath(
                "//ul[@class=\"navigation-menu-items initialized\"]/li/a[contains(text(), '" + topMenuName + "')]");

        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(topMenuElement)).build().perform();

        By subHeaderElements = By.xpath("//li/a[contains(text(), '" + topMenuName + "')]/../div/ul/li/a");
        return driver.findElements(subHeaderElements);
    }

    public void clickOnSubHeaderElement(List<WebElement> subHeaderElements, String itemName) {
        Optional<WebElement> webElement = subHeaderElements.stream().
                filter(element -> element.getText().equalsIgnoreCase(itemName)).findFirst();

        if (webElement.isPresent()) {
            webElement.get().click();
        } else {
            throw new NoSuchElementException("Element not find");
        }
    }
}
