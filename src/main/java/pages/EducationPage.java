package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class EducationPage extends BasePage {

    private By headerName = By.xpath("//div[contains(@class,\"hero-banner\")]//h1[3]/span");
    private By subjectElements = By.xpath("//div[@class=\"side-panel\"]/ul/li/a");

    public EducationPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public String getHeaderName() {
        return driver.findElement(headerName).getText().trim();
    }

    public List<WebElement> getSubjectElements() {
        return driver.findElements(subjectElements);
    }
}
