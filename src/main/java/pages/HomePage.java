package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage {

    private By headerName = By.xpath("//div[@class=\"banner-heading\"]");

    public HomePage(WebDriver driver) {
        super(driver);
    }

    @Override
    public String getHeaderName() {
         return driver.findElement(headerName).getText().trim();
    }
}
