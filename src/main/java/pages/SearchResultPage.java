package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

public class SearchResultPage {

    private WebDriver driver;

    public SearchResultPage(WebDriver driver) {
        this.driver = driver;
    }

    private By resultItemsTitle = By.xpath("//h3[@class=\"product-title\"]//a");
    private By resultItems = By.xpath("//section[@class=\"product-item \"]");
    private By searchButton = By.xpath("//div[@class=\"input-group\"]//button");
    private By searchInput = By.xpath("//div[@class=\"input-group\"]//input");

    public List<WebElement> getResultItems() {
        return driver.findElements(resultItems);
    }

    public List<WebElement> getResultItemsTitle() {
        return driver.findElements(resultItemsTitle);
    }

    public List<WebElement> getAddToCartButtons() {
        return getResultItems().stream()
                .map(element -> element.findElement(By.xpath("//button")))
                .collect(Collectors.toList());
    }

    public void clickOnSearchButton() {
        driver.findElement(searchButton).submit();
    }

    public void addTextToSearchInput(String text) {
        WebElement element = driver.findElement(searchInput);
        element.clear();
        element.sendKeys(text);
    }


}
