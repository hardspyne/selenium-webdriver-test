package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class StudentPage extends BasePage{

    private By headerName = By.xpath("//div[contains(@class,\"hero-banner\")]//p[@class=\"sg-title-h1\"]//span[1]");
    private By learnMoreLink = By.xpath("//span[contains(text(),\"Learn More\")]/../..");

    public StudentPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public String getHeaderName() {
        return driver.findElement(headerName).getText().trim();
    }

    public List<WebElement> getLearnMoreElements() {
        return driver.findElements(learnMoreLink);
    }
}
