import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.EducationPage;
import pages.HomePage;
import pages.SearchResultPage;
import pages.StudentPage;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MainTest {
    private WebDriver driver;
    private HomePage homePage;
    private StudentPage studentPage;
    private EducationPage educationPage;
    private WebDriverWait webDriverWait;
    private SearchResultPage searchResultPage;


    private String[] topMenuLinksName = {"WHO WE SERVE", "SUBJECTS", "ABOUT"};
    private String[] whoWeServeItemsName = {"Students", "Instructors", "Book Authors", "Professionals",
            "Researchers", "Institutions", "Librarians", "Corporations", "Societies", "Journal Editors",
            "Government", "Bookstores"};
    private String[] subjectsItemsName = {"Information & Library Science", "Education & Public Policy",
            "K-12 General", "Higher Education General", "Vocational Technology",
            "Conflict Resolution & Mediation (School settings)", "Curriculum Tools- General",
            "Special Educational Needs", "Theory of Education", "Education Special Topics",
            "Educational Research & Statistics", "Literacy & Reading", "Classroom Management"};

    @Before
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.wiley.com/en-ru");

        webDriverWait = new WebDriverWait(driver, 10);

        homePage = new HomePage(driver);
        studentPage = new StudentPage(driver);
        educationPage = new EducationPage(driver);
        searchResultPage = new SearchResultPage(driver);
    }

    @Test
    public void mainTest() {
        checkHomePage();
        checkStudentsPage();
        checkEducationPage();
        checkSearchOnHomePage();
        checkSearchResultPage();
    }

    private void checkHomePage() {
        Assert.assertTrue(homePage.getCurrentUrl().matches("https://www\\.wiley\\.com/en-.{2}"));

        List<WebElement> topMenuElements = homePage.getTopMenuElements();

        //Check links in the top menu
        Assert.assertTrue(isEveryItemDisplayed(topMenuElements));
        Assert.assertTrue(isEveryItemHasRightName(topMenuLinksName, topMenuElements));

        List<WebElement> whoWeServeItems = homePage.getSubHeaderElements("WHO WE SERVE");

        webDriverWait.until(ExpectedConditions.visibilityOfAllElements(whoWeServeItems));
        //Check items under Who We Serve
        Assert.assertTrue(isEveryItemDisplayed(whoWeServeItems));
        //There are 12 items under resources sub-header
        Assert.assertEquals(12, whoWeServeItems.size());
        Assert.assertTrue(isEveryItemHasRightName(whoWeServeItemsName, whoWeServeItems));
        //go to student page
        homePage.clickOnSubHeaderElement(whoWeServeItems, "Students");
    }

    private void checkStudentsPage() {
        Assert.assertTrue(studentPage.getCurrentUrl().matches("https://www\\.wiley\\.com/en-.{2}/students"));

        Assert.assertTrue(studentPage.getHeader().isDisplayed());
        Assert.assertEquals(studentPage.getHeaderName(), "Students");

        List<WebElement> learnMoreElements = studentPage.getLearnMoreElements();

        Assert.assertTrue(isEveryItemDisplayed(learnMoreElements));
        Assert.assertTrue(isEveryLinkDirectToRightUtl("www.wileyplus.com", learnMoreElements));

        List<WebElement> subjectsItems = studentPage.getSubHeaderElements("SUBJECTS");
        //wait visibility only 14 elements in popup
        webDriverWait.until(ExpectedConditions.visibilityOfAllElements(subjectsItems.subList(0, 14)));
        //go to education page
        studentPage.clickOnSubHeaderElement(subjectsItems, "Education");
    }

    private void checkEducationPage() {
        webDriverWait.until(ExpectedConditions.urlMatches("https://www\\.wiley\\.com/en-.{2}/Education.+"));

        Assert.assertTrue(educationPage.getHeader().isDisplayed());
        Assert.assertEquals(educationPage.getHeaderName(), "Education");

        List<WebElement> subjectItems = educationPage.getSubjectElements();

        //Check items under subjects
        Assert.assertEquals(13, subjectItems.size());
        Assert.assertTrue(isEveryItemHasRightName(subjectsItemsName, subjectItems));
        //go to home page
        educationPage.clickOnLogo();
    }

    private void checkSearchOnHomePage() {
        webDriverWait.until(ExpectedConditions.urlMatches("https://www\\.wiley\\.com/en-.{2}"));

        Assert.assertTrue(studentPage.getHeader().isDisplayed());

        homePage.clickOnSearchButton();
        //nothing happens if not enter anything in the search input
        Assert.assertTrue(homePage.getCurrentUrl().matches("https://www\\.wiley\\.com/en-.{2}"));
        Assert.assertTrue(homePage.getHeader().isDisplayed());

        homePage.addTextToSearchInput("Java");

        webDriverWait.until(ExpectedConditions.visibilityOf(homePage.getSearchResultArea()));

        Assert.assertTrue(homePage.getSearchResultArea().isDisplayed());

        List<WebElement> searchSuggestions = homePage.getSearchSuggestions();
        List<WebElement> productsSectionItems = homePage.getProductsSectionItems();

        Assert.assertEquals(4, searchSuggestions.size());
        //On the “Products” section, there are 4 titles
        Assert.assertEquals(4, productsSectionItems.size());
        Assert.assertTrue(isEveryItemStartWithWord("Java", searchSuggestions));
        Assert.assertTrue(isEveryItemContainsWord("Java", productsSectionItems));

        homePage.clickOnSearchButton();
    }

    private void checkSearchResultPage() {
        List<WebElement> resultItems = searchResultPage.getResultItems();
        List<String> textForListItems = getTextForWebElements(resultItems);

        webDriverWait.until(ExpectedConditions.visibilityOfAllElements(resultItems));

        Assert.assertEquals(10, searchResultPage.getResultItems().size());
        Assert.assertTrue(isEveryItemContainsWord("Java", searchResultPage.getResultItemsTitle()));
        Assert.assertEquals(10, searchResultPage.getAddToCartButtons().size());

        searchResultPage.addTextToSearchInput("Java");
        searchResultPage.clickOnSearchButton();

        List<WebElement> newResultItems = searchResultPage.getResultItems();
        List<String> textForNewListItems = getTextForWebElements(newResultItems);
        //check text in the result elements
        Assert.assertEquals(textForListItems, textForNewListItems);
    }

    private boolean isEveryItemHasRightName(String[] itemsName, List<WebElement> items) {
        return items.stream()
                .map(element -> element.getAttribute("innerHTML").replaceAll("amp;", "").trim())
                .collect(Collectors.toList()).containsAll(Arrays.asList(itemsName));
    }

    private boolean isEveryItemDisplayed(List<WebElement> items) {
        return items.stream().allMatch(WebElement::isDisplayed);
    }

    private boolean isEveryLinkDirectToRightUtl(String url, List<WebElement> items) {
        return items.stream()
                .map(element -> element.getAttribute("href"))
                .allMatch(element -> element.contains(url));
    }

    private boolean isEveryItemStartWithWord(String world, List<WebElement> items) {
        //get parameter in result url and then check that result start with world
        return items.stream()
                .map(element -> element.getAttribute("href").split("=")[1])
                .allMatch(element -> element.toLowerCase().startsWith(world.toLowerCase()));
    }

    private boolean isEveryItemContainsWord(String world, List<WebElement> items) {
        return items.stream()
                .map(element -> element.getAttribute("innerHTML"))
                .allMatch(element -> element.toLowerCase().contains(world.toLowerCase()));
    }

    private List<String> getTextForWebElements(List<WebElement> items) {
        return items.stream()
                .map(WebElement::getText).collect(Collectors.toList());
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
